<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_Comment_comment</name>
   <tag></tag>
   <elementGuidId>4a4e16cc-5123-42e5-a8e9-56d915a88c1f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#txt_comment</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id='txt_comment']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>e02ee5de-52d3-4061-9e45-a10d2b1efa82</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>0d2ef81c-3db9-4a4f-b5e6-94223ba62294</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>txt_comment</value>
      <webElementGuid>5dee0453-9cc3-4ada-af1c-141522b81236</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>comment</value>
      <webElementGuid>68539223-ef76-4341-9012-9618112e8443</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Comment</value>
      <webElementGuid>ab4b66c4-f4a7-4bce-b299-a165718605cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>rows</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>8e49c731-6a95-46e3-a053-09da7d41582e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;txt_comment&quot;)</value>
      <webElementGuid>4808e3cc-9398-493c-a82b-4adb95f2a924</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//textarea[@id='txt_comment']</value>
      <webElementGuid>80a624cc-35d2-4214-871a-3eda3b2a838a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[5]/div/textarea</value>
      <webElementGuid>51492197-0665-4b0a-a32f-cc55e9781d44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>1aea5182-9475-4fc2-91be-4b77dbbc77cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@id = 'txt_comment' and @name = 'comment' and @placeholder = 'Comment']</value>
      <webElementGuid>76854b15-dbb7-47d9-9aad-07559c523c2f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
