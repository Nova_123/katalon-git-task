<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_CURA Healthcare_menu-toggle</name>
   <tag></tag>
   <elementGuidId>86b4b0e3-7d98-49f3-9177-9d6a1c910199</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#menu-toggle</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='menu-toggle']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>eae25346-e2a2-48cd-a7ca-ca3477e168fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>menu-toggle</value>
      <webElementGuid>a126d372-5fac-47db-b55b-d9435d7c2db5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>47b2545b-4f8b-4a23-ae3b-0a98b2b82dbd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-dark btn-lg toggle</value>
      <webElementGuid>aedd14fe-6e98-48c4-8804-e531438529d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;menu-toggle&quot;)</value>
      <webElementGuid>b4abeff4-f495-4795-9ee0-43cad1fa42a3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='menu-toggle']</value>
      <webElementGuid>e0570400-82bc-455b-b533-33d1bc638098</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CURA Healthcare'])[1]/preceding::a[2]</value>
      <webElementGuid>557a9119-b112-4711-b2d9-b8dac083fdc2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/preceding::a[3]</value>
      <webElementGuid>c59a1048-5d36-443f-a671-a5c3806d7404</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '#')]</value>
      <webElementGuid>18b0ba79-3348-4754-ab28-fd605e1306c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a</value>
      <webElementGuid>5ec2a1af-6a32-400b-8cee-62c845ed8577</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'menu-toggle' and @href = '#']</value>
      <webElementGuid>226b95e7-864d-451e-9844-1989e9f2c6d8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
